import React from 'react';

import './spinner.css';

export default function Spinner() {
  return (
    <div className="loadingio-spinner-double-ring-f2flai340st">
      <div className="ldio-vve8khvyvro">
        <div></div>
        <div></div>
        <div><div></div></div>
        <div><div></div></div>
      </div>
    </div>
  );
}