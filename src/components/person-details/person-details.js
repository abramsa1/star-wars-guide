import React, { useState, useEffect } from 'react';

import SwapiService from '../../services/swapi-service';
import PersonView from './person-view';
import Spinner from '../spinner';
import './person-details.css';

export default function PersonDetails({ personId }) {
  const swapiService = new SwapiService();
  const [person, setPerson] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (personId) {
      setLoading(true);
      swapiService.getPerson(personId)
      .then(person => setPerson(person));
    }
    setLoading(false);
  }, [personId]);

  return (
    <div className="person-details card">
      {loading ? <Spinner /> : null}
      {!loading && !person ? <span>Select a person from a list</span> : null }
      {!loading && person ? <PersonView personInfo={person} /> : null}
    </div>
  );
}
