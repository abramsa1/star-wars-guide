import React, {useEffect, useState, useRef} from 'react';

import PlanetView from './planet-view';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';
import SwapiService from '../../services/swapi-service';
import './random-planet.css';

export default function RandomPlanet() {
  const swapiService = new SwapiService();

  const [planetInfo, setPlanetInfo] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const interval = useRef(null);

  const onPlanetLoaded = (planet) => {
    setPlanetInfo(planet);
    setLoading(false);
  };

  const onError = (err) => {
    setLoading(false);
    setError(true);
  }

  const updatePlanet = () => {
    setLoading(true);
    const id = Math.floor(Math.random() * 25 + 3);
    swapiService.getPlanet(id)
      .then(onPlanetLoaded)
      .catch(onError);   
  };

  useEffect(() => {
    updatePlanet();
    interval.current = setInterval(updatePlanet, 10000);

    return () => clearInterval(interval.current);
  }, []);

  return (
    <div className="random-planet jumbotron rounded">
      {error ? <ErrorIndicator /> : null}
      {loading ? <Spinner /> : null}
      {!(loading || error) ? <PlanetView planetInfo={planetInfo} /> : null}  
    </div>
  );
}
