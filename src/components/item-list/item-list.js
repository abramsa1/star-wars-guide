import React, { useState, useEffect } from 'react';

import SwapiService from '../../services/swapi-service';
import './item-list.css';

export default function ItemList({ onItemSelected }) {
  const swapiService = new SwapiService();
  const [peopleList, setPeopleList] = useState(null);

  useEffect(() => {
    swapiService.getAllPeople()
      .then(people => setPeopleList(people))
  }, []);

  const renderItems = (peopleList) => {
    return peopleList.map(({ id, name }) => {
      return (
        <li 
          className="list-group-item" 
          key={id}
          onClick={() => onItemSelected(id)}
        >
          {name}
        </li>
      );
    })
  }

  return (
    <ul className="item-list list-group">
      { peopleList ? renderItems(peopleList) : null }
{/*       <li className="list-group-item">
        Luke Skywalker
      </li>
      <li className="list-group-item">
        Darth Vader
      </li>
      <li className="list-group-item">
        R2-D2
      </li> */}
    </ul>
  );
}
